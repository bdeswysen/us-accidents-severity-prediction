# __Accident severity prediction considering weather conditions and nearby road development__
---------------------------
This work focuses on the construction of a machine learning model able to predict the severity of an accident depending on environment conditions when it happened (weather, proximity of road development, visibility conditions , etc.).

---------------------------
## Motivation:


__36120__.

This is the (estimated) number of people   died  in  motor  vehicle  traffic  crashes in 2019,  according to the US Department of Transportation (https://crashstats.nhtsa.dot.gov/Api/Public/ViewPublication/812946). Although this number has been declining in recent years, this cause of death remains the most important of healthy American citizens.

The reaction capacities of emergency services and other services concerned with road traffic, car accidents and their impact can be combined with computerized assistance systems. Imagine that such a system exists and makes it possible to predict the consequences of an accident in real time in order to react and prevent the situation from worsening or, better, anticipating its appearance. It is with this in mind that this work was carried out: contribute to the development of decision support tools to support people in charge of road management and, maybe, save lives.

More specifically, this work focuses on the construction of a machine learning model able to predict the severity of an accident depending on environment conditions when it happened (weather, proximity of road development, visibility conditions , etc.). As a first approach, a supervised one was implemented through a "decision tree" classification.

---------------------------
## __Research question:__

> Can we use decision tree classification to predict accident severity considering weather conditions and nearby road developement?


---------------------------

## __Research method:__

### __A. Analysis at 2 different geographical scales:__
In general, this car accidents analysis is carried out according to two different scales:

1. Macroscopic analysis on the global dataset covering __US territory__.
2. Analysis at the scale of a geographic regions: the state of __California__.

This two different geographical scales approach offers an overview at two different granularity levels:
- The macroscopic __national approach__ is generic and consider all measured events as equivalent throughout the United States.
- The __regional approach__ is more specific to a geographic region (California) and try to identify patterns specific to this region such as: weather and climate of the region, driving behavior of drivers, proportions of the different types of vehicles on the road (motorcycles, trucks, bicycles, ...) or the types of roads and road facilities.


Consequently, most of the operations in this analysis are performed in duplicate: on the global set and on the Californian set.


### __B. Analysis of car accident context and potential determining factors__

During this step we evaluate the different available measures to better understand the context of car accident.

1. Accident severity criteria analysis
2. Weather conditions analysis
3. Car accidents spatial distribution analysis


### __C. Classification and predictions using decision tree__

In this last step, we try to predict the severity of car accidents considering some features determining the context. These features are selected in the weather measures and the road development proximity.

Given that we already know the categories we want to predict (= severity criteria from 1 to 4), we are in a ```Supervised approach```. The targeted value of prediction is a category considering quantitative input data, we are then in a ```classification``` problem. As a first approach of the question, the chosen algorithm here is the ```decision tree```, studied in this introduction course.

---------------------------
